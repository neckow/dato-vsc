# Descriptive Algorithm & Transcoding Operation

Dato est un langage de programmation français basé sur le langage algorithmique ! Basé sur Ada, il permet d'exécuter et de compiler des programmes simples par des types entiers et booléens, idéal pour commencer la programmation !

Dato est le fruit d'un travail de projet de premier semestre, réalisé par **DASSIEU Damien** et **FROMENT Tom**

## Dépendance

Ce projet dépend de gnat afin de pouvoir compiler du code Ada.
Un setup d'installation est proposé via l'extension pour les utilisateurs Linux et Windows.

## Installation

### Par Visual Studio Code (Code source & Extension)

Dans le **Marketplace**, recherchez Dato et installez l'application :

L'extension vous permettra de bénéficier d'une coloration syntaxique pour les fichiers .dato, mais également de commandes de compilation automatique :



### Par le projet Git (Code source uniquement)

Les sources de **Dato** sont publiques et disponibles via son [Projet Git](https://gitlab.com/dassieu.damien/dato/-/releases)

Après installation des sources, le projet peut ensuite être compilé :

```sh
# Compilation des sources
./compile
```

Enfin, un fichier dato peut être exécuté :

```sh
# Exécution simple
./dist/main <chemin relatif ou absolu vers le fichier dato>

# Exécution avec écriture du fichier intermédiaire
./dist/main -I <chemin relatif ou absolu vers le fichier dato>

# Exécution en debug
./dist/main -D <chemin relatif ou absolu vers le fichier dato>
```

## Langage

### Syntaxe

### Erreurs


**Enjoy!**
