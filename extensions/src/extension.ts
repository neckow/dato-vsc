import * as vscode from 'vscode';
import * as os from 'os';


export function activate(context: vscode.ExtensionContext) {

	/* -- USER PLATFORM -- */
	const PLATFORM = os.platform();

	/* -- PATH OPTION -- */
	const WORKDIR = context.asAbsolutePath(".");
	const PS_WORKDIR = "$env:USERPROFILE\\.vscode\\extensions\\dato\\dato\\dist"
	const SEPARATOR = PLATFORM == 'win32' ? '\\' : '/'
	const INTPRDIR = `${WORKDIR}${SEPARATOR}dato`;
	const INSTALL = `${WORKDIR}${SEPARATOR}install`;
	const COMPILE = `${INTPRDIR}${SEPARATOR}compile`;
	const INTRPRE = `src${SEPARATOR}main.adb`;
	const DIST = `${INTPRDIR}${SEPARATOR}dist${SEPARATOR}main`;


	const statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 1000);
	statusBarItem.command = 'dato.buildAndRun';
	statusBarItem.name = "Dato run";
	statusBarItem.text = "$(debug-start) Dato run";
	statusBarItem.tooltip = "Run and Build a Dato algorithm";
	context.subscriptions.push(statusBarItem);

	// Run Default
	context.subscriptions.push( vscode.commands.registerCommand("dato.runDefault", () => open()));

	// Generate Toda File
	context.subscriptions.push( vscode.commands.registerCommand("dato.runAndToda", () => open(true)));

	// Debug
	context.subscriptions.push( vscode.commands.registerCommand("dato.runAndDebug", () => open(true,true)));


	// Ouvrir un processus fils
	function checkGnat(): Promise<Status> {

		return exec(`gnatmake -v`, "Gnat not found");

	}


	//			>>>>>>>>>>>>>>>>>>>>>>>>>	GNAT INSTALLATION   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<

	async function installGnatForLinux(): Promise<Status> {

		const password = await vscode.window.showInputBox({
			password: true,
			placeHolder: "Root Password",
			prompt: "GNAT Missing ! Please enter your password to install it.",
		});

		return exec(`echo ${password} | sudo -S ${INSTALL}${SEPARATOR}linux.sh`, "Incorrect password");

	}

	async function installGnatForWindows(): Promise<Status> {

		let status;

		/* Check for admin rights */
		status = await exec(`setx /M path "%path%"`, "Visual Studio Code must be run in privileged mode");
		if (!status.state) return status;

		status = null;

		vscode.window.showInformationMessage("Downloading binaries...");

		/* Download Gnat */
		status = await exec(`cd ${INSTALL} & windows.bat`, "Failed to get Gnat file online");
		if (!status.state) return status;

		console.log("Status : " + status.state);
		status = null

		vscode.window.showInformationMessage("Waiting for user install...");

		/* Install Gnat */
		status = await exec(`cd ${INSTALL} & gnat.exe & del gnat.exe"`, "An error as occured while processing");
		if (!status.state) return status;

		console.log("Status : " + status.state);
		status = null

		/* Check for PATH variable */
		status = await exec(`setx /M path "C:\\GNAT\\2021\\bin;%path% & gnatmake -v`, "gnatmake still not found. Have you changed the default location folder? If so, add the current location to PATH system variable.");

		console.log("Status : " + status.state);

		/* Restart Visual Studio Code */
		let response = await vscode.window.showInformationMessage("Visual must be restarted to apply modifications");

		vscode.commands.executeCommand("workbench.action.closeWindow");

		return status;
	}


	//			>>>>>>>>>>>>>>>>>>>>>>>>>	GNAT COMPILATION    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<


	async function compileFileForLinux(): Promise<Status> {

		return exec(`cd ${INTPRDIR} ; ${COMPILE} ${INTRPRE}`, "Error while compiling the program");

	}

	async function compileFileForWindows(): Promise<Status> {

		return exec(`cd ${INTPRDIR} & compile.bat ${INTRPRE}`, "Unknown error while compiling the program");

	}

	//			>>>>>>>>>>>>>>>>>>>>>>>>>	GNAT EXECUTION    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<


	async function execDatoForLinux(filePath: string | undefined, generateTodaFile? : Boolean, enableDebug? : Boolean) {

		const argToda = generateTodaFile ? "-I " : ""
		const argDebug = enableDebug == true ? "-D " : ""

		if(generateTodaFile) {
			await exec(`${DIST} -I ${filePath}`,""); // Generate the file before the execution for the user
		}

		const terminal = (vscode.window.terminals.length < 0) ? vscode.window.createTerminal() : vscode.window.terminals[0];

		terminal.show();
		terminal.sendText(`${DIST} ${argToda}${argDebug}${filePath}`);

		if(generateTodaFile) {
			//vscode.commands.executeCommand("workbench.action.newGroupRight");
			const todaFilePath = (filePath?.substring(0,filePath.length-4)+"toda");
			vscode.workspace.openTextDocument(vscode.Uri.parse(todaFilePath)).then(doc => {
				vscode.window.showTextDocument(doc,vscode.ViewColumn.Beside);
			})
		}

		

	}

	async function execDatoForWindows(filePath: string | undefined, generateTodaFile? : Boolean, enableDebug? : Boolean) {

		const terminal = (vscode.window.terminals.length < 0) ? vscode.window.createTerminal() : vscode.window.terminals[0];
		const argToda = generateTodaFile ? "-I " : ""
		const argDebug = enableDebug ? "-D " : ""

		if(generateTodaFile) {
			await exec(`${DIST}.exe -I ${filePath}`,""); // Generate the file before the execution for the user
		}

		terminal.show();
		terminal.sendText(`${DIST}.exe ${argToda}${argDebug}${filePath}`);

		if(generateTodaFile) {
			const todaFilePath = filePath?.substring(0,filePath.length-4)+"toda";
			vscode.window.showInformationMessage(todaFilePath);
			vscode.workspace.openTextDocument(vscode.Uri.parse(todaFilePath)).then(doc => {
				vscode.window.showTextDocument(doc,vscode.ViewColumn.Beside);
			})
		}


	}

	//			>>>>>>>>>>>>>>>>>>>>>>>>>	OPEN    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<


	async function openwithLinux(filePath: string | undefined, generateTodaFile? : Boolean, enableDebug? : Boolean) {

		let installed = await checkGnat();

		if (!installed.state) {
			let response = await vscode.window.showInformationMessage("Ada is not installed on your system.\n Do you want to install it?", "Yes", "No");
			if (response == "No") return;
			let installationSucces = await installGnatForLinux();
			if (!installationSucces.state) vscode.window.showInformationMessage(installationSucces.msg ? installationSucces.msg : "");
			return;
		}

		let compilationSucces = await compileFileForLinux();
		if (!compilationSucces.state) { vscode.window.showInformationMessage(compilationSucces.msg ? compilationSucces.msg : ""); return; }
		let s = await execDatoForLinux(filePath,generateTodaFile, enableDebug);

	}

	async function openwithWindows(filePath: string | undefined, generateTodaFile? : Boolean, enableDebug? : Boolean) {

		let installed = await checkGnat();

		if (!installed.state) {
			let response = await vscode.window.showInformationMessage("Ada is not installed on your system.\n Do you want to install it?", "Yes", "No");
			if (response == "No" || response == undefined) return;
			let installationSucces = await installGnatForWindows();
			if (!installationSucces.state) {
				vscode.window.showInformationMessage(installationSucces.msg ? installationSucces.msg : "")
			}
			return;

		}

		let compilationSucces = await compileFileForWindows();
		if (!compilationSucces.state) { vscode.window.showInformationMessage(compilationSucces.msg ? compilationSucces.msg : ""); return; }
		let s = await execDatoForWindows(filePath, generateTodaFile, enableDebug);



	}

	async function openwithMac() {

	}


	async function open(generateTodaFile? : Boolean, enableDebug? : Boolean) {

		statusBarItem.show();

		const filePath = PLATFORM == 'win32' ? vscode.window.activeTextEditor?.document.uri.fsPath : vscode.window.activeTextEditor?.document.uri.path;



		switch (PLATFORM) {
			case "win32":
				openwithWindows(filePath, generateTodaFile, enableDebug);
				break;
			case "darwin":
				openwithMac();
				break;
			default:
				openwithLinux(filePath, generateTodaFile, enableDebug);
				break;
		}


	}


	context.subscriptions.push(statusBarItem);

}

export class Status {

	state: boolean;
	msg?: string;

	constructor(state: boolean, msg?: string) {
		this.state = state;
		this.msg = msg;
	}
}

export function exec(command: string, error: string): Promise<Status> {

	const cp = require('child_process');

	return new Promise((resolve) => {

		cp.exec(command, (err: any, stdout: any, stderr: any) => {
			if (err) { console.log(err); vscode.window.showInformationMessage(error); resolve(new Status(false, err)); } else { resolve(new Status(true)) }
		});

	})

}



// this method is called when your extension is deactivated
export function deactivate() { }
