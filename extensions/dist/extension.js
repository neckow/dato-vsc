/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ([
/* 0 */,
/* 1 */
/***/ ((module) => {

module.exports = require("vscode");

/***/ }),
/* 2 */
/***/ ((module) => {

module.exports = require("os");

/***/ }),
/* 3 */
/***/ ((module) => {

module.exports = require("child_process");

/***/ })
/******/ 	]);
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.deactivate = exports.exec = exports.Status = exports.activate = void 0;
const vscode = __webpack_require__(1);
const os = __webpack_require__(2);
function activate(context) {
    /* -- USER PLATFORM -- */
    const PLATFORM = os.platform();
    /* -- PATH OPTION -- */
    const WORKDIR = context.asAbsolutePath(".");
    const PS_WORKDIR = "$env:USERPROFILE\\.vscode\\extensions\\dato\\dato\\dist";
    const SEPARATOR = PLATFORM == 'win32' ? '\\' : '/';
    const INTPRDIR = `${WORKDIR}${SEPARATOR}dato`;
    const INSTALL = `${WORKDIR}${SEPARATOR}install`;
    const COMPILE = `${INTPRDIR}${SEPARATOR}compile`;
    const INTRPRE = `src${SEPARATOR}main.adb`;
    const DIST = `${INTPRDIR}${SEPARATOR}dist${SEPARATOR}main`;
    const statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 1000);
    statusBarItem.command = 'dato.buildAndRun';
    statusBarItem.name = "Dato run";
    statusBarItem.text = "$(debug-start) Dato run";
    statusBarItem.tooltip = "Run and Build a Dato algorithm";
    context.subscriptions.push(statusBarItem);
    // Run Default
    context.subscriptions.push(vscode.commands.registerCommand("dato.runDefault", () => open()));
    // Generate Toda File
    context.subscriptions.push(vscode.commands.registerCommand("dato.runAndToda", () => open(true)));
    // Debug
    context.subscriptions.push(vscode.commands.registerCommand("dato.runAndDebug", () => open(true, true)));
    // Ouvrir un processus fils
    function checkGnat() {
        return exec(`gnatmake -v`, "Gnat not found");
    }
    //			>>>>>>>>>>>>>>>>>>>>>>>>>	GNAT INSTALLATION   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    async function installGnatForLinux() {
        const password = await vscode.window.showInputBox({
            password: true,
            placeHolder: "Root Password",
            prompt: "GNAT Missing ! Please enter your password to install it.",
        });
        return exec(`echo ${password} | sudo -S ${INSTALL}${SEPARATOR}linux.sh`, "Incorrect password");
    }
    async function installGnatForWindows() {
        let status;
        /* Check for admin rights */
        status = await exec(`setx /M path "%path%"`, "Visual Studio Code must be run in privileged mode");
        if (!status.state)
            return status;
        status = null;
        vscode.window.showInformationMessage("Downloading binaries...");
        /* Download Gnat */
        status = await exec(`cd ${INSTALL} & windows.bat`, "Failed to get Gnat file online");
        if (!status.state)
            return status;
        console.log("Status : " + status.state);
        status = null;
        vscode.window.showInformationMessage("Waiting for user install...");
        /* Install Gnat */
        status = await exec(`cd ${INSTALL} & gnat.exe & del gnat.exe"`, "An error as occured while processing");
        if (!status.state)
            return status;
        console.log("Status : " + status.state);
        status = null;
        /* Check for PATH variable */
        status = await exec(`setx /M path "C:\\GNAT\\2021\\bin;%path% & gnatmake -v`, "gnatmake still not found. Have you changed the default location folder? If so, add the current location to PATH system variable.");
        console.log("Status : " + status.state);
        /* Restart Visual Studio Code */
        let response = await vscode.window.showInformationMessage("Visual must be restarted to apply modifications");
        vscode.commands.executeCommand("workbench.action.closeWindow");
        return status;
    }
    //			>>>>>>>>>>>>>>>>>>>>>>>>>	GNAT COMPILATION    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    async function compileFileForLinux() {
        return exec(`cd ${INTPRDIR} ; ${COMPILE} ${INTRPRE}`, "Error while compiling the program");
    }
    async function compileFileForWindows() {
        return exec(`cd ${INTPRDIR} & compile.bat ${INTRPRE}`, "Unknown error while compiling the program");
    }
    //			>>>>>>>>>>>>>>>>>>>>>>>>>	GNAT EXECUTION    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    async function execDatoForLinux(filePath, generateTodaFile, enableDebug) {
        const argToda = generateTodaFile ? "-I " : "";
        const argDebug = enableDebug == true ? "-D " : "";
        if (generateTodaFile) {
            await exec(`${DIST} -I ${filePath}`, ""); // Generate the file before the execution for the user
        }
        const terminal = (vscode.window.terminals.length < 0) ? vscode.window.createTerminal() : vscode.window.terminals[0];
        terminal.show();
        terminal.sendText(`${DIST} ${argToda}${argDebug}${filePath}`);
        if (generateTodaFile) {
            //vscode.commands.executeCommand("workbench.action.newGroupRight");
            const todaFilePath = (filePath?.substring(0, filePath.length - 4) + "toda");
            vscode.workspace.openTextDocument(vscode.Uri.parse(todaFilePath)).then(doc => {
                vscode.window.showTextDocument(doc, vscode.ViewColumn.Beside);
            });
        }
    }
    async function execDatoForWindows(filePath, generateTodaFile, enableDebug) {
        const terminal = (vscode.window.terminals.length < 0) ? vscode.window.createTerminal() : vscode.window.terminals[0];
        const argToda = generateTodaFile ? "-I " : "";
        const argDebug = enableDebug ? "-D " : "";
        if (generateTodaFile) {
            await exec(`${DIST}.exe -I ${filePath}`, ""); // Generate the file before the execution for the user
        }
        terminal.show();
        terminal.sendText(`${DIST}.exe ${argToda}${argDebug}${filePath}`);
        if (generateTodaFile) {
            const todaFilePath = filePath?.substring(0, filePath.length - 4) + "toda";
            vscode.window.showInformationMessage(todaFilePath);
            vscode.workspace.openTextDocument(vscode.Uri.parse(todaFilePath)).then(doc => {
                vscode.window.showTextDocument(doc, vscode.ViewColumn.Beside);
            });
        }
    }
    //			>>>>>>>>>>>>>>>>>>>>>>>>>	OPEN    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    async function openwithLinux(filePath, generateTodaFile, enableDebug) {
        let installed = await checkGnat();
        if (!installed.state) {
            let response = await vscode.window.showInformationMessage("Ada is not installed on your system.\n Do you want to install it?", "Yes", "No");
            if (response == "No")
                return;
            let installationSucces = await installGnatForLinux();
            if (!installationSucces.state)
                vscode.window.showInformationMessage(installationSucces.msg ? installationSucces.msg : "");
            return;
        }
        let compilationSucces = await compileFileForLinux();
        if (!compilationSucces.state) {
            vscode.window.showInformationMessage(compilationSucces.msg ? compilationSucces.msg : "");
            return;
        }
        let s = await execDatoForLinux(filePath, generateTodaFile, enableDebug);
    }
    async function openwithWindows(filePath, generateTodaFile, enableDebug) {
        let installed = await checkGnat();
        if (!installed.state) {
            let response = await vscode.window.showInformationMessage("Ada is not installed on your system.\n Do you want to install it?", "Yes", "No");
            if (response == "No" || response == undefined)
                return;
            let installationSucces = await installGnatForWindows();
            if (!installationSucces.state) {
                vscode.window.showInformationMessage(installationSucces.msg ? installationSucces.msg : "");
            }
            return;
        }
        let compilationSucces = await compileFileForWindows();
        if (!compilationSucces.state) {
            vscode.window.showInformationMessage(compilationSucces.msg ? compilationSucces.msg : "");
            return;
        }
        let s = await execDatoForWindows(filePath, generateTodaFile, enableDebug);
    }
    async function openwithMac() {
    }
    async function open(generateTodaFile, enableDebug) {
        statusBarItem.show();
        const filePath = PLATFORM == 'win32' ? vscode.window.activeTextEditor?.document.uri.fsPath : vscode.window.activeTextEditor?.document.uri.path;
        switch (PLATFORM) {
            case "win32":
                openwithWindows(filePath, generateTodaFile, enableDebug);
                break;
            case "darwin":
                openwithMac();
                break;
            default:
                openwithLinux(filePath, generateTodaFile, enableDebug);
                break;
        }
    }
    context.subscriptions.push(statusBarItem);
}
exports.activate = activate;
class Status {
    constructor(state, msg) {
        this.state = state;
        this.msg = msg;
    }
}
exports.Status = Status;
function exec(command, error) {
    const cp = __webpack_require__(3);
    return new Promise((resolve) => {
        cp.exec(command, (err, stdout, stderr) => {
            if (err) {
                console.log(err);
                vscode.window.showInformationMessage(error);
                resolve(new Status(false, err));
            }
            else {
                resolve(new Status(true));
            }
        });
    });
}
exports.exec = exec;
// this method is called when your extension is deactivated
function deactivate() { }
exports.deactivate = deactivate;

})();

module.exports = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=extension.js.map