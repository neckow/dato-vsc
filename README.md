# Descriptive Algorithm & Transcoding Operation

Dato est un langage de programmation français basé sur le langage algorithmique ! Basé sur Ada, il permet d'exécuter et de compiler des programmes simples par des types entiers et booléens, idéal pour commencer la programmation !

Dato est le fruit d'un travail de projet de premier semestre, réalisé par **DASSIEU Damien** et **FROMENT Tom**

[[_TOC_]]

## Dépendance

Ce projet dépend de gnat afin de pouvoir compiler du code Ada.
Un setup d'installation est proposé via l'extension pour les utilisateurs Linux et Windows.

## Installation

### Par Visual Studio Code (Code source & Extension)

Dans le **Marketplace**, recherchez Dato et installez l'application :

![Extension Dato sur Visual Studio Code](doc/img/extension.png)

L'extension vous permettra de bénéficier d'une coloration syntaxique pour les fichiers .dato et .toda

![Coloration syntaxique en Dato](doc/img/syntax.png)

Ainsi que de commandes de compilation automatique :

![Commandes Dato sur Visual Studio Code](doc/img/commands.png)

### Par le projet Git (Code source uniquement)

Les sources de **Dato** sont publiques et disponibles via son [Projet Git](https://gitlab.com/dassieu.damien/dato/-/releases)

Après installation des sources, le projet peut ensuite être compilé :

```sh
# Compilation des sources
./compile
```

Enfin, un fichier dato peut être exécuté :

```sh
# Exécution simple
./dist/main <chemin relatif ou absolu vers le fichier dato>

# Exécution avec écriture du fichier intermédiaire
./dist/main -I <chemin relatif ou absolu vers le fichier dato>

# Exécution en debug
./dist/main -D <chemin relatif ou absolu vers le fichier dato>
```

## Commande

| Options | Commande | Equivalent VsCode | Rôle
| ----------- | ----------- | ----------- | ----------- |
|   | main \<chemin/fichier/dato> | Dato Run | Exécuter un programme Dato
| -I  | main -I \<chemin/fichier/dato> | Generate .toda file | Exécuter un programme Dato et générer un fichier en pseudo-assembleur (.toda) |
| -D  | main -D \<chemin/fichier/dato> | Debug File (-I implicite) | Afficher l'état de la mémoire à chaque intruction |

## Exemple de programme

```

-- Ce programme calcule la factorielle F d’un entier n

Programme Facto est

    n : Entier -- Entier naturel pour lequel le programme calcule la factorielle

    Fact : Entier -- entier naturel résultat du calcul de Factorielle (n)

    i : Entier -- entier naturel, variable intermédiaire

Début

    n <- 5

    i <- 1

    Fact <- 1

    Tant Que i <= n Faire 

        Fact <- Fact * i 

        i <- i + 1

    Fin Tant Que

    Afficher("Fact de " + n + " : " + Fact)
    Retour

Fin

```

D'autres exemples peuvent être trouvées dans le dossier [sample](./doc/sample/)

## Langage

### Syntaxe

#### **Entête de programme**
<a name="entete"></a>

| Syntaxe | Rôle |
| ----------- | ----------- | 
| Programme \<Nom Programme> est | Entête |

#### **Déclaration de variables**
| Syntaxe | Rôle |
| ----------- | ----------- | 
| \<Nom Variable> : Entier | Déclaration d'une variable entière |

#### **Types**
<a name="types"></a>
| Syntaxe | Domaines | 
| ----------- | ----------- | 
| Entier | $`[–2^{15}+1;+2^{15}–1]`$  |
| Booleen | {FAUX, VRAI} ou {0, 1} |

#### **Corps de programme**
<a name="corps"></a>

| Syntaxe | Rôle |
| ----------- | ----------- | 
| Début \| Debut | Début du programme |
| Fin | Fin du programme |

#### **Affectation entière** 
<a name="affectation"></a>

| Syntaxe | Exemple | Rôle |
| ----------- | ----------- | ----------- | 
|  Variable <- <[Entier](#types)>| i <- 47 | Affectation d'un entier |
|  Variable <- <[Entier](#types)> <[op](#operation)> <[Entier](#types)> | i <- 3 * 4 | Affectation du résultat d'une opération |


#### **Affectation booléenne** 
<a name="affectation"></a>

| Syntaxe | Exemple | Rôle |
| ----------- | ----------- | ----------- | 
|  Variable <- <[Booleen](#types)>| i <- VRAI | Affectation d'un booléen |
|  Variable <- <[Booleen](#types)> <[op](#operation-booleenne)> <[Booleen](#types)> | i <- VRAI OU FAUX | Affectation du résultat d'une opération booléenne |
|  Variable <- <[Entier](#types)> <[comp](#comparaison)> <[Entier](#types)> | i <- 3 != 4 | Affectation du résultat d'une opération booléenne |


#### **Opération entière**
<a name="operation"></a>

| Syntaxe | Rôle |
| ----------- | ----------- | 
|  + | Addition |
|  - | Soustraction |
|  * | Multiplication |
|  / | Division |

#### **Opération booléenne**
<a name="operation-booleenne"></a>

| Syntaxe | Rôle |
| ----------- | ----------- | 
|  OU \| + | OU logique |
|  ET \| * | Et logique |



#### **Comparaison**
<a name="comparaison"></a>

| Syntaxe | Rôle |
| ----------- | ----------- | 
|  <= | Inférieur ou égal |
|  < | Strictement inférieur |
|  == | Egal |
|  != | Différent |
|  >= | Supérieur ou égal |
|  > | Strictement supérieur |

#### **Condition**
<a name="condition"></a>

| Syntaxe | Rôle |
| ----------- | ----------- | 
|  Si [Booleen](#comparaison) Alors | Exécute un bloc de code si la condition booléenne est vérifiée |
|  Sinon | Exécute le bloc de code suivant si la condition ne l'est pas (indique la fin du bloc "Si") |
|  Fin Si | Fermeture du bloc "Si" (ou "Sinon" si existant)

#### **Boucle**
<a name="boucle"></a>

| Syntaxe | Rôle |
| ----------- | ----------- | 
|  Tant que [Booleen](#comparaison) Faire | Exécute un bloc de code itérativement tant que la condition booléenne est vérifiée |
|  Fin Tant Que | Fermeture du bloc "Tant Que"


### Erreurs

| Intitulés | Description |
| ----------- | ----------- | 
| **File Extension Exception** | Le fichier n'est pas un .dato ou un .toda |
| **Too Much Argument Exception** | La commande contient trop d'arguments|
| **Undeclared Variable Exception** | Une variable n'existe pas|
| **Bad Usage Exception** | Mauvais usage d'une focntion |
| **Ended Error Exception** | La fin du programme est appelé avant son début  |
| **Started Error Exception** | Le programme n'a pas encore commencé |
| **Debuted Error Exception** | La déclaration des variables n'est pas terminée |
| **Compile Exception** | Syntaxe incorrecte |
| **Condition Exception** | Plusieurs conditions sont traitées dans la même séquence |
| **Operation Exception** | Plusieurs opérations sont traitées dans la même séquence |
| **Operator Exception** | L'opérateur utilisé n'existe pas ou n'est pas attribuable à ce type de données |
| **Type Not Found Exception** | Le type de données n'existe pas |
| **Domain Exception** | Une affectation à une variable est invalide et sort du domaine de son type. |
| **Var Not Init Exception** | Une variable est utilisée avant avoir été affectée (valeur indéfinie) |
| **Already Declared Exception** | Une variable a été déclarée deux fois |
| **Declaration Exception** | Une variable est déclarée à un emplacement invalide |
| **Bad Name Exception** | Une variable contient des caractères invalides |
| **Declaration Exception** | Une variable est déclarée à un emplacement invalide |

**Enjoy!**
